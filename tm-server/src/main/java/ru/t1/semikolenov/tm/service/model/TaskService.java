package ru.t1.semikolenov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.semikolenov.tm.api.service.model.ITaskService;
import ru.t1.semikolenov.tm.exception.field.EmptyDescriptionException;
import ru.t1.semikolenov.tm.exception.field.EmptyIdException;
import ru.t1.semikolenov.tm.exception.field.EmptyNameException;
import ru.t1.semikolenov.tm.exception.field.EmptyUserIdException;
import ru.t1.semikolenov.tm.model.Task;
import ru.t1.semikolenov.tm.repository.model.TaskRepository;
import ru.t1.semikolenov.tm.repository.model.UserRepository;

import java.util.Date;
import java.util.List;

@Service
public class TaskService extends AbstractUserOwnedService<Task> implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository repository;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Override
    protected TaskRepository getRepository() {
        return repository;
    }

    @NotNull
    private UserRepository getUserRepository() {
        return userRepository;
    }

    @Nullable
    @Override
    @Transactional
    public Task create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable Task task = new Task();
        task.setName(name);
        task.setUser(getUserRepository().findById(userId).orElse(null));
        @NotNull final TaskRepository repository = getRepository();
        repository.save(task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(getUserRepository().findById(userId).orElse(null));
        @NotNull final TaskRepository repository = getRepository();
        repository.save(task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(getUserRepository().findById(userId).orElse(null));
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        @NotNull final TaskRepository repository = getRepository();
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final TaskRepository repository = getRepository();
        return repository.findAllByUserIdAndProjectId(userId, projectId);
    }

}
